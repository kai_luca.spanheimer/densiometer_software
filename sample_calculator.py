#!/usr/bin/env python3

#weights in	wt_particles/wt_susp
weights=(0.0025,0.005,0.0075,0.01)
#suspension in g
susp_per_test=2

total_particles=0

for weight in weights:
	total_particles+=susp_per_test*weight

stem_solution=total_particles/max(weights)
print("particles needed:"+str(total_particles)+"g\nstem solution created: "
	+str(stem_solution)+"g")

count=0
for i in weights:
	if i!=max(weights):
		stem_weight=susp_per_test*i/max(weights)
		print("For suspension "+str(count)+ " with weight part "+str(i) +"g/g\nmix "
			+ str(stem_weight)+"g of the stem with solvent so its 2g in total")
	else:
		print("For suspension "+str(count)+ " with weight part "+str(i) +"g/g\njust take the stem")
	count+=1
