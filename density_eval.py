#!/usr/bin/env python3
"""This module is used to evaluate density data"""

import csv
import sys
import getopt
import os
import readline
import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize

def linear_function(offset:float)->callable:
    """Returns a linear function with the given offset"""
    return lambda x,m:offset+m*x

class InputCompleter():
    """This is an input completer from the readline module"""
    def __init__(self,options):
        self.options=sorted(options)
        self.matches=[]

    def complete(self,text,state):
        """Completing the input started typing"""
        if state==0:
            if text:
                self.matches=[s for s in self.options if s and s.startswith(text)]
            else:
                self.matches=self.options[:]
        try:
            return self.matches[state]
        except IndexError:
            return None

class DensityData:
    """A class to structure the densiometer Data as i type it to my database"""
    def __init__(self,filepath):
        self.name=filepath.split("/")[-2]
        self.offset=0
        with open(filepath) as csvfile:
            intern_data=[]
            reader=csv.reader(csvfile)
            for counter,row in enumerate(reader):
                if row!=[]:
                    if row[0][0]=='#':
                        if counter==0:
                            row[0]=row[0][1:]
                            self.data_types=row
                        else:
                            continue
                    else:
                        if percent_to_float(row[0])==0:
                            self.offset=float(row[1])
                        intern_data.append(row)
            data_array=np.empty([len(intern_data),len(self.data_types)],float)
            self.error_bars=np.ones([len(intern_data),len(self.data_types)],float)*0.0001
            for i,date in enumerate(intern_data):
                for j,_ in enumerate(self.data_types):
                    data_array[i,j]=percent_to_float(date[j])
            self.rho=data_array


    def draw_data(self,outfile:str)->None:
        """Just draws the data as an errorbar plot"""
        fig=plt.figure()
        axe=fig.add_subplot(111)
        axe.errorbar(self.rho[:,0],self.rho[:,1],self.error_bars[:,0],linestyle="")
        plt.savefig(outfile)

    def draw_rho_fit(self,outfile:str)->None:
        """Fits the density from the data. Works in a limited range only,
        since it's nonlinear for higher percentage."""
        fig=plt.figure()
        axe=fig.add_subplot(111)
        plt.title(self.name)
        axe.errorbar(self.rho[:,0],self.rho[:,1],self.error_bars[:,0],marker="x",linestyle="")
        y_func=linear_function(self.offset)
        slope,slope_err=optimize.curve_fit(y_func,self.rho[:,0],self.rho[:,1])
        slope,slope_err=slope[0],slope_err[0][0]
        print(slope,slope_err)
        rang=[min(self.rho[:,0]),max(self.rho[:,0])]
        x_arr=np.arange(rang[0],rang[1]+(rang[1]-rang[0])/100,(rang[1]-rang[0])/100)
        y_arr=self.offset+slope*x_arr
        axe.plot(x_arr,y_arr)
        axe.set_xlabel("Gewichtsanteil Kolloid")
        axe.set_ylabel("Dichte Suspenion")
        plt.rc('text',usetex=True)
        rho_part=r'''$\Rightarrow \rho=$'''
        rho=self.offset**2/(self.offset-slope)
        rho_string=f"{rho:.4f}"
        axe.annotate("m="+f'{slope:.5f}'+"\n"+r"$\pm$"
                        +f'{slope_err:.5f}'
                        +"\n"+rho_part +rho_string,
                        xy=(x_arr[50],y_arr[50]),
                        xytext=(1.25*x_arr[50],y_arr[50]+(y_arr[0]-y_arr[100])*0.2))
        plt.savefig(outfile)
        return np.asarray((rho))

    def draw_v_fit(self,outfile:str)->np.array:
        """Draws a fit of the specific volume to the Data"""
        fig=plt.figure()
        axe=fig.add_subplot(111)
        axe.set_title(self.name.replace("_","-"))
        volume=1/self.rho[:,1]
        delta_v=self.error_bars[:,0]/(self.rho[:,1]**2)
        axe.errorbar(self.rho[:,0],volume,delta_v,linestyle="",marker="x",label="Messwerte")
        y_func=linear_function(volume[0])
        slope,slope_err=optimize.curve_fit(y_func,np.array(self.rho[:,0]),np.array(volume))
        slope,slope_err=slope[0],slope_err[0][0]
        rho=1/(volume[0]+slope)
        rang=[min(self.rho[:,0]),max(self.rho[:,0])]
        x_arr=np.arange(rang[0],rang[1]+(rang[1]-rang[0])/100,(rang[1]-rang[0])/100)
        y_arr=volume[0]+slope*x_arr
        axe.plot(x_arr,y_arr)
        axe.set_xlabel("Gewichtsanteil Kolloid")
        axe.set_ylabel(r'''$\frac{1}{\rho}$''')
        plt.rc('text',usetex=True)
        rho_part=r'''$\Rightarrow \rho=$'''
        axe.annotate("m="+f'{slope:.5f}'+"\n"+r"$\pm$"
                    +f'{slope_err:5f}'
                    +"\n"+rho_part +f"{rho:.4f}",
            xy=(x_arr[50],y_arr[50]),
            xytext=(1.25*x_arr[50],y_arr[50]+(y_arr[0]-y_arr[100])*0.2))
        plt.savefig(outfile)
        return np.asarray((rho))

def write_data_file(outfile:str,rho_density:float,rho_volume:float):
    """Writes both fitted densities into the oufile"""
    with open(outfile,"w") as o_file:
        o_file.write(f"Density by Density fit is: {rho_density:.5f}\n")
        o_file.write(f"Density by Density fit is: {rho_volume:.5f}\n")

def percent_to_float(string:str)->float:
    """Reads the percentage as string and returns float"""
    out=0.
    percent=False
    for i in string:
        if i=="%":
            string=string.split("%")[0]
            percent=True
    out=float(string)
    if percent:
        out*=0.01
    return out

def setup_completion(directory:str):
    """Reads the subdirectories of the given directory and sets up the setup_completion"""
    directories=[x[0] for x in os.walk(directory) if
        (x[0]!=directory and not x[0].startswith(directory+"/Densiometer_software"))]
    print(directories)
    for counter,dire in enumerate(directories):
        if len(dire.split("/"))>1:
            directories[counter]=dire.split("/")[1]
    print("Possible samples:")
    for dire in directories:
        print("	"+dire)
    completer=InputCompleter(directories)
    readline.set_completer(completer.complete)
    readline.parse_and_bind('tab:complete')

if __name__=="__main__":
    DIRECTORY=".."
    argv=sys.argv[1:]
    opts,args=getopt.getopt(argv,"hi:","indir=")

    TIPING=True
    for (opt,arg) in opts:
        if opt=="-h":
            print("""Run the programm with option -i <Directory>,
                  if you dont want it to be interactive""")
        elif opt in ("-i","--indir"):
            sample=arg
            TIPING=False
    if TIPING:
        setup_completion(DIRECTORY)
        sample=input("sample name:\n(press <TAB> for autocomplete)\n")
    data=DensityData(DIRECTORY+"/"+sample+"/density.csv")
    rho_outfile=DIRECTORY+"/"+sample+"/density_plot.pdf"
    v_outfile=DIRECTORY+"/"+sample+"/specific_volume_plot.pdf"
    data_outfile=DIRECTORY+"/"+sample+"/evaluated_density.txt"

    rho_dense=data.draw_rho_fit(rho_outfile)
    print("Rho fit done:",rho_dense,"g/l")
    rho_vol=data.draw_v_fit(v_outfile)
    print("V fit done",rho_vol,"g/l")
    write_data_file(data_outfile,rho_dense,rho_vol)
